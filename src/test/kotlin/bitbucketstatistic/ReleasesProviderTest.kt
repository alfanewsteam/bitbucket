package bitbucketstatistic

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import java.text.SimpleDateFormat
import java.util.*
import java.text.DateFormatSymbols
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoField
import java.time.temporal.TemporalAccessor
import java.time.temporal.TemporalQuery
import java.util.Locale


internal class ReleasesProviderTest {

    @Test
    fun getFormatter() {
//        val date = "\"27 февр. 2018 г. 11:22\""

        val date = "\"27 фев 2018 г. 11:22\""
        val expected = 22L
//        val formatter = SimpleDateFormat("\"dd MMM. yyyy г. HH:mm\"", Locale("Ru"))

        val formatter = DateTimeFormatter.ofPattern("\"dd MMM['.'] yyyy 'г.' HH:mm\"", Locale("ru"))
        val actual = formatter.parseBest(date,
                TemporalQuery { ZonedDateTime.from(it) },
                TemporalQuery { LocalDateTime.from(it) },
                TemporalQuery { LocalDate.from(it) }
        )
        assertEquals(expected, actual.getLong(ChronoField.MINUTE_OF_HOUR))
    }
}