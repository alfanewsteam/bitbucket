package cloneall

import com.cdancy.bitbucket.rest.BitbucketClient
import com.cdancy.bitbucket.rest.domain.project.Project
import com.cdancy.bitbucket.rest.domain.repository.Repository
import com.cdancy.bitbucket.rest.features.ProjectApi
import com.cdancy.bitbucket.rest.features.RepositoryApi
import java.io.File
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

fun main(args: Array<String>) {
    CloneAll().cloneAllProjects()
}

class CloneAll {
    val executor = Executors.newCachedThreadPool()

    fun getProjectApi(): ProjectApi = BitbucketClient.builder()
            .endPoint("http://git/")
            .token("MzIyOTQwNzAzNzQzOv/6zHY9bBW+Ej/FVdM41ulehNp5")
            .build()
            .api()
            .projectApi()

    fun getRepositoryApi(): RepositoryApi = BitbucketClient.builder()
            .endPoint("http://git/")
            .token("MzIyOTQwNzAzNzQzOv/6zHY9bBW+Ej/FVdM41ulehNp5")
            .build()
            .api()
            .repositoryApi()


    fun cloneAllProjects() {
        getProjectApi()
                .list(null, null, 0, 500)
                .values()
                .forEach {
                    cloneProject(it)
                }
    }

    private fun cloneProject(project: Project) {
        getRepositoryApi()
                .list(project.key(), 0, 500)
                .values()
                .forEach {
                    cloneRepo(it)
                }
    }

    private fun cloneRepo(repo: Repository) {
        val url = repo.links()
                .clone()
                .first { it["name"] == "http" }
                .get("href")
        val projectName = repo.project().name()
        val repoName = repo.name()

        if (url != null) {
            val process = createProcess(url, projectName, repoName)
            executeProcess(executor, process, repoName)
        } else {
            System.err.println("ERROR repo url is NULL $projectName $repoName")
        }
    }

    fun executeProcess(executor: ExecutorService, process: Process, reponame:String) {
        val streamGobbler = StreamGobbler(process.inputStream, { println(it) })
        executor.submit(streamGobbler)
        val exitCode = process.waitFor()
        if (exitCode != 128){
            println("!!!!exit code $exitCode $reponame")
        }else{
            println("exit code $exitCode $reponame")
        }

    }

    fun createProcess(repoUrl: String, projectName: String, repoName: String): Process {
        return ProcessBuilder()
                .command("sh", "-c", "git clone $repoUrl ${prepareName(projectName)}/${prepareName(repoName)}")
//                .directory(File(System.getProperty("user.home")+"/clone"))
                .directory(File("/Volumes/Untitled/Anton/AlfaSources"))
                .start()
    }

    fun prepareName(value:String):String{
        return value
                .replace(' ', '_')
                .replace('.', '_')
                .replace('(', '{')
                .replace(')', '}')
    }
}