package bitbucketstatistic

import java.util.*

data class ReleaseDto(
        val version: Version,
        val fullReleaseDate: Date,
        val fromDate: Date?,
        val toDate: Date?
)