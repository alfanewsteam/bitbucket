package bitbucketstatistic

import java.text.SimpleDateFormat
import java.util.*


class ReleasesProvider {

    private val formatter = SimpleDateFormat("\"dd MMM yyyy 'г.' HH:mm\"", Locale("ru"))
    private val cvsSplitBy = ","


    fun loadReleases(releasesFilename: String, mergesFilename: String): Collection<ReleaseDto> {
        val mergeStream = ReleasesProvider::class.java.getResourceAsStream(mergesFilename).bufferedReader()

        val mergePrs = mergeStream.lineSequence()
                .map { it.split(cvsSplitBy) }
                .map {
                    val version = parseVersionDto(it[0])
                    val date = formatter.parse(it[1])
                    Pair(version, date)
                }
                .toList()


        val lastBuildPerRelease = mergePrs
                .sortedBy { it.second }
                .associate { it }


        val firstBuildPerRelease = mergePrs
                .asIterable()
                .sortedByDescending { it.second }
                .associate { it }

        val releasesFromKeys = firstBuildPerRelease.keys.sorted()

        return lastBuildPerRelease
                .mapValues {
                    var datePrFrom: Date? = null
                    var datePrTo: Date? = null

                    val currentKeyIndex = releasesFromKeys.indexOf(it.key)
                    datePrTo = firstBuildPerRelease[releasesFromKeys[currentKeyIndex]]

                    val prevKeyIndex = currentKeyIndex - 1
                    if (prevKeyIndex in 0 until releasesFromKeys.size) {
                        datePrFrom = firstBuildPerRelease[releasesFromKeys[prevKeyIndex]]
                    }
                    ReleaseDto(it.key, it.value, datePrFrom, datePrTo)
                }
                .values

    }
}