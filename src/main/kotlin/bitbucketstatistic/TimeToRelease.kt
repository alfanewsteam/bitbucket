package bitbucketstatistic

import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


fun main(args: Array<String>) {
    val averageAm = getAverage(
            "AM",
            "am-android",
            "/am_full_release.csv",
            "/am_merge_to_master.csv")
    println("am = " + averageAm)

    val averageAbm = getAverage(
            "ABM",
            "abm-android",
            "/abm_full_release.csv",
            "/abm_merge_to_master.csv")
    println("abm = " + averageAbm)
}

private fun getAverage(project: String, repo: String, releasesFilename: String, mergesFilename: String): Double {
    println("-----------" + project + "---------------")
    val prProvider = PrProvider()
    val prsToDev = prProvider.getMergedToDevPr(project, repo, getDateFrom())

    val releasesProvider = ReleasesProvider()
    val releases = releasesProvider.loadReleases(releasesFilename, mergesFilename)

    return getPrsWithRelease(prsToDev, releases)
            .filter { it.lifetime != null }
            .map { it.lifetime!! }
            .average()
}

private fun getPrsWithRelease(prsToDev: List<PrDto>, releases: Collection<ReleaseDto>): List<PrDto> {
    return prsToDev
            .map {
                val currentRelease = getReleaseByPrDto(it, releases)
                val releaseDate = currentRelease?.fullReleaseDate
                var timeToRelease: Long? = null
                if (releaseDate != null) {
                    timeToRelease = (releaseDate.time - it.mergeDate.time) / 1000 / 60 / 60 / 24
                }
                it.apply {
                    lifetime = timeToRelease
                    release = currentRelease
                }
            }
            .removeDuplicates()
            .sortedBy { it.mergeDate }
}

fun Iterable<PrDto>.removeDuplicates(): List<PrDto> {
    val result = HashMap<Pair<String, ReleaseDto?>, PrDto>()
    this.onEach {
        result.put(Pair(it.branchName, it.release), it)
    }
    return result.values.toMutableList()
}


private fun getReleaseByPrDto(pr: PrDto, releases: Collection<ReleaseDto>): ReleaseDto? {
    return releases.firstOrNull {
        val from = it.fromDate?.time ?: 0
        val to = it.toDate?.time ?: 0
        pr.mergeDate.time in from until to
    }
}

private fun getDateFrom(): Long {
    val oneMonthEgo = Calendar.getInstance()
    oneMonthEgo.add(Calendar.MONTH, -3)
    return oneMonthEgo.timeInMillis
}

