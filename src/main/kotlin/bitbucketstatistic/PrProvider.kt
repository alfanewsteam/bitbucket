package bitbucketstatistic

import com.cdancy.bitbucket.rest.BitbucketClient
import com.cdancy.bitbucket.rest.domain.pullrequest.PullRequest
import com.cdancy.bitbucket.rest.features.PullRequestApi
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class PrProvider {

    private val api: PullRequestApi = BitbucketClient.builder()
            .endPoint("http://git/")
            .token("MzIyOTQwNzAzNzQzOv/6zHY9bBW+Ej/FVdM41ulehNp5")
            .build()
            .api()
            .pullRequestApi()

    fun getMergedToDevPr(
            project: String,
            repo: String,
            dateFrom: Long,
            dateTo: Long = System.currentTimeMillis()
    ): List<PrDto> {
        return request(project, repo)
                .filter {
                    it.updatedDate() in dateFrom..dateTo
                }
                .filter { it.fromRef().displayId().contains("feature", true) }
                .map { PrDto(it.author().user().displayName(), it.fromRef().displayId(), Date(it.updatedDate())) }
    }


    private fun request(project: String, repo: String): List<PullRequest> {
        val result = ArrayList<PullRequest>()
        result.addAll(singleRequest(project, repo, 0))
        result.addAll(singleRequest(project, repo, 100))
        result.addAll(singleRequest(project, repo, 200))
        return result
    }

    private fun singleRequest(project: String, repo: String, offset: Int): List<PullRequest> {
        return api.list(project, repo, null,
                "refs/heads/development", "merged", null,
                null, null, offset, 100).values()
    }
}