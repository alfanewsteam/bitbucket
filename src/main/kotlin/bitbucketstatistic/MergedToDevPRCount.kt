package bitbucketstatistic

import java.util.Calendar


fun main(args: Array<String>) {
    val prProvider = PrProvider()
    val abmCount = prProvider.getMergedToDevPr("ABM", "abm-android", getDateFrom()).size
    println("abmCount " + abmCount)
    val amCount = prProvider.getMergedToDevPr("AM", "am-android", getDateFrom()).size
    println("amCount " + amCount)
}

private fun getDateFrom(): Long {
    val oneMonthEgo = Calendar.getInstance()
    oneMonthEgo.add(Calendar.MONTH, -3)
    return oneMonthEgo.timeInMillis
}


