package bitbucketstatistic

fun parseVersionDto(from: String): Version {
    val splitedVersion = from.split('.').map { it.toInt() }
    return Version(splitedVersion[0], splitedVersion[1])
}

data class Version(
        var major: Int,
        var minor: Int
) : Comparable<Version> {

    override fun compareTo(other: Version): Int {
        val majorCompare = this.major.compareTo(other.major)
        val minorCompare = this.minor.compareTo(other.minor)

        return when {
            majorCompare != 0 -> majorCompare
            else -> minorCompare
        }
    }
}

