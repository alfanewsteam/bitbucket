package bitbucketstatistic

import java.util.*

data class PrDto(
        val name: String,
        val branchName: String,
        val mergeDate: Date,
        var lifetime: Long? = null,
        var release: ReleaseDto? = null
)
